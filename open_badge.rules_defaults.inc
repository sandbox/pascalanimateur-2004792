<?php
/**
 * @file
 * open_badge.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function open_badge_default_rules_configuration() {
  $items = array();
  $items['rules_badge_award'] = entity_import('rules_config', '{ "rules_badge_award" : {
      "LABEL" : "Badge award",
      "PLUGIN" : "rule",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : {
        "badge" : { "label" : "Badge", "type" : "node" },
        "user" : { "label" : "User", "type" : "user" }
      },
      "IF" : [
        { "node_is_of_type" : { "node" : [ "badge" ], "type" : { "value" : { "badge" : "badge" } } } }
      ],
      "DO" : [
        { "list_add" : {
            "list" : [ "badge:field-recipients" ],
            "item" : [ "user" ],
            "unique" : 1,
            "pos" : "start"
          }
        }
      ]
    }
  }');
  $items['rules_badge_revoke'] = entity_import('rules_config', '{ "rules_badge_revoke" : {
      "LABEL" : "Badge revoke",
      "PLUGIN" : "rule",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : {
        "badge" : { "label" : "Badge", "type" : "node" },
        "user" : { "label" : "User", "type" : "user" }
      },
      "IF" : [
        { "node_is_of_type" : { "node" : [ "badge" ], "type" : { "value" : { "badge" : "badge" } } } }
      ],
      "DO" : [
        { "list_remove" : { "list" : [ "badge:field-recipients" ], "item" : [ "user" ] } }
      ]
    }
  }');
  return $items;
}
