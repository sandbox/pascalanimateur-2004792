<?php
/**
 * @file
 * open_badge.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function open_badge_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'badges';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Badges';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'grid';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['label'] = 'No badges found';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'No badges awarded to this user.';
  $handler->display->display_options['empty']['area']['format'] = 'filtered_html';
  /* Field: Content: Image */
  $handler->display->display_options['fields']['field_badge_image']['id'] = 'field_badge_image';
  $handler->display->display_options['fields']['field_badge_image']['table'] = 'field_data_field_badge_image';
  $handler->display->display_options['fields']['field_badge_image']['field'] = 'field_badge_image';
  $handler->display->display_options['fields']['field_badge_image']['label'] = '';
  $handler->display->display_options['fields']['field_badge_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_badge_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_badge_image']['settings'] = array(
    'image_style' => 'badge',
    'image_link' => 'content',
  );
  /* Contextual filter: Content: Recipients (field_recipients) */
  $handler->display->display_options['arguments']['field_recipients_target_id']['id'] = 'field_recipients_target_id';
  $handler->display->display_options['arguments']['field_recipients_target_id']['table'] = 'field_data_field_recipients';
  $handler->display->display_options['arguments']['field_recipients_target_id']['field'] = 'field_recipients_target_id';
  $handler->display->display_options['arguments']['field_recipients_target_id']['default_action'] = 'default';
  $handler->display->display_options['arguments']['field_recipients_target_id']['default_argument_type'] = 'user';
  $handler->display->display_options['arguments']['field_recipients_target_id']['default_argument_options']['user'] = FALSE;
  $handler->display->display_options['arguments']['field_recipients_target_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_recipients_target_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_recipients_target_id']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'badge' => 'badge',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->display->display_options['path'] = 'user/%/badges';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Badges';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'user-menu';
  $handler->display->display_options['menu']['context'] = 1;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $export['badges'] = $view;

  return $export;
}
