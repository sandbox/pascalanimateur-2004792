/**
 * @file Open Badge Designer jQuery 
 */

(function ($) { 
  Drupal.behaviors.openbadgedesigner = { 
    attach: function(context, settings) { 
      // open the designer
      openDesigner = function(){
        URL = 'https://www.openbadges.me/designer.html?origin=';
        URL = URL+Drupal.settings.openbadgedesigner.origin;
        URL = URL+'&email='
        URL = URL+Drupal.settings.openbadgedesigner.issuer_email;
        URL = URL+'&close=true';
        options = 'width=1015,height=680,location=0,menubar=0,status=0,toolbar=0';
        var designerWindow = window.open(URL,'',options);
      }
      // handle the response
      window.onmessage = function(e){
        if(e.origin=='https://www.openbadges.me'){
          if(e.data!='cancelled') {
            $(Drupal.settings.openbadgedesigner.filedata_id).val(e.data);
            $(Drupal.settings.openbadgedesigner.filedata_id).trigger('blur');            
          }
          else
            designerWindow.close();
        }
      };
    } 
  }; 
})(jQuery);
