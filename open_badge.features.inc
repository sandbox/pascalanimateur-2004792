<?php
/**
 * @file
 * open_badge.features.inc
 */

/**
 * Implements hook_views_api().
 */
function open_badge_views_api() {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function open_badge_image_default_styles() {
  $styles = array();

  // Exported image style: badge.
  $styles['badge'] = array(
    'name' => 'badge',
    'effects' => array(
      1 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 200,
          'height' => 200,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function open_badge_node_info() {
  $items = array(
    'badge' => array(
      'name' => t('Badge'),
      'base' => 'node_content',
      'description' => t('Badges are awarded to users according to a specific criteria.'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
  );
  return $items;
}
